console.log('Hello World');


function details(fName, lName, age){
    console.log(fName + ' ' + lName + ' is ' + age + ' years of age.');
}
details('John', 'Smith', 30);


function returnValue(){
    console.log('This was printed out inside a function')
    return true;
}
let isMarried = returnValue();
console.log("The value of isMarried is: " +  isMarried);