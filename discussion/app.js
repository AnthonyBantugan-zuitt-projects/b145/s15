// lets include a confrim message
// console.log() is used to 
console.log('Hello from JS');
//There are multiple ways to display messages in the console using different formats.

// error message
console.error('Error Something');

// warning message
console.warn('Oops Warning')

//syntax: 
    // function nameOfFunction() {
    //     // instructions/procedures
    //     retun
    // }

// lets create a function that will display an error
function errorMessage() {
    console.error('Error Oh no!!!!');
}
errorMessage(); //callout the function




function greetings() { //DECLARATION
    //procedures
    console.log('Salutation from JavaScipt');
}
//INVOCATION/CALLING-OUT
greetings();

//There are different methods in  declaring a function in JS

//[SECTION 1] Variables declared outside a function can be sed INSIDE a function

let givenName = 'John';
let familyName = 'Smith';
//lets create a function that will utilize the information outside its scope.
function fullName(){
    //combine the values of the info outside and display it inside the console.
    console.log(givenName + ' ' + familyName);
}
//Invoking/Calling out functions we do it, by parethesis() after the fucntion name.
fullName();



//[SECTION 2] "blocked scope" variables, this means that variables can only be used within the scope of the function.

//lets crate a function that will allow us to compute the values of 2 variables.
function computeTotal() {
    let numA = 20;
    let numB = 5;
    //add the values and display it inside the console.
    console.log(numA + numB);
}
//console.log(numA); <== //the variable cannot be used outside the scope of the function where it was declared.
//call out the function
computeTotal();


//[SECTION 3] Functions with Parameters

//"Parameter" -> acts as a variable or a container/catchers that only exist inside a function. A paramete is used to store information that is provided to a function when it is called or invoked.

//lets create a function that emulate a pokemon battle
function pokemon(pangalan) {
    //were going to use the "parameter" declared on this function to be processed and displayed inside the console.
    console.log('I Choose you: ' + pangalan);
}
//invocation
pokemon('Pikachu');

//parameters VS arguments?

//Arguments -> is the "ACTUAL" value that is provided inside a function for it to work properly. The term ARGUMENT is used when functions are called/invoked as compared to parameters when a function is simply declared.

//[SECTION 4] Functions with multiple parameters

//lets declare the function that will get the sum of mutiple values.
function addNumbers(numA, numB, numC, numD, numE) {
    console.log(numA + numB + numC + numD + numE); 
}
//NaN: Not a Number
addNumbers(1,2,3,4,5); //15

//lets create a function that will generate a person's full name

function createFullName(fName, mName, lName) {
    console.log(lName + ', ' + fName + ' ' + mName);
}

//invoke the function and pass down the arguments needed that will take the place of each parameter.
createFullName('Juan', 'Dela', 'Cruz');

//upon using multiple arguments it will correspond to the number of "parameters" declared in a function in succeeding order, unless reordered inside a function.



//[SECTION 5] Using Variables as Arguments
//create a function that will display the stats of a pokemon in a battle.
let attackA = 'Tackle';
let attackB = 'Thundebolt';
let attackC = 'Quick Attack';
let selectedPokemon = 'Ratata';
function pokemonStats(name, attack) {
    console.log(name + ' use ' + attack);
}
pokemonStats(selectedPokemon, attackB);
//Ratata use Quick Attack

//Note: using variables as arguments will allow us to utilize code reusability, this will help us reduce the amount of code that we have to write down.

//the use of the "return" expression/statement 
    //the return statement is used to display the output of a function

//[SECTION 6] The RETURN statement

    //the "return" statement allows the output of a function to be passed to the line/block of code that called the function

//create a function that will return a string message.
function retrunString() {
     'Hello World';
     return 2 + 1; //3
}
// lets repackage the result of this function inside a new variable.
let functionOutput = retrunString();
console.log(functionOutput);

//other option: console.log(returnString());

// lets create a simple function to demonstrate the use and behavior of the return statement again.
function dialog(){
    console.log('Oops! I didi it again');
    console.log("Dont you know that you're toxic");
    return 'Im a slave for you!';
    console.log('Sometimes I run!');
}

//invocation
console.log(dialog());

//note: any block/line of code that will come after our "return" statement is going to be ignored because it came after the end of the function execution.
//The main purpose of the return statement is to identify which will be the final output/result of the function and at the same time determine the end of the function statement.


//[SECTION 7] Using Functions as Arguments
//=> Function parameters can also accept other functions as their argument.
//=> Some complex functions use other functions as their arguments to perform more complicated/complex results.

//were just goint to create a simple function to be used as an argument later.
function argumentSaFunctions(){
    console.log('This function was passed as an argument');
}

function invokeFunction(argumentNaFunctions, pangalawangFunction) {
    argumentNaFunctions();
    pangalawangFunction(selectedPokemon,attackB);
    pangalawangFunction(selectedPokemon, attackC);
}

//invoke the function above
invokeFunction(argumentSaFunctions,pokemonStats);

//IF YOU WANT TO SEE details/information about a function
console.log(invokeFunction); //finding information about a function inside the console.
